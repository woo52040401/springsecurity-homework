1.src/main/java/com.tgl.mvc.App.java 為主程式

2.使用Swagger UI測試

3.在cmd使用 mvn spring-boot:run 啟動

4.程式啟動後，打開瀏覽器輸入網址: http://localhost:8080/swagger-ui.html 能進行測試

5.帳號密碼共兩組: (1) User(帳號:user 密碼:user)
                   (2) Admin(帳號:admin 密碼:admin)

6.User部分只提供查詢，Admin所有功能都能操作
                   
7.Batch Insert 格式: 中文姓名,英文姓名,身高,體重,Email,Phone    舉例如下:
    
    老王,Nam-ghor,190,80,w22ff3@gmail.com,1232
    
    張三,Na-s,172,62,w22eee3@gmail.com,5532