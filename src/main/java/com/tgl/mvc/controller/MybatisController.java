package com.tgl.mvc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tgl.mvc.exception.RecordNotFoundException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.MybatisService;

@Controller
@ResponseBody
@RequestMapping(value = "/rest/mybatis")
public class MybatisController {
	
	@Autowired
	private MybatisService mybatisService;
	private static final Logger LOG = LogManager.getLogger(MybatisController.class);
	
	@PostMapping(value = "/insert")
	public ResponseEntity<Long> insert(@RequestBody @Valid Employee employee){
		mybatisService.insert(employee);
		return new ResponseEntity<>(employee.getId(), HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Long> delete(@PathVariable("id") @Min(1) long employeeId){
		Employee result = mybatisService.findById(employeeId);
		if(result == null) {
			throw new RecordNotFoundException(employeeId);
		}
		mybatisService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}
	
	@PutMapping(value = "/update")
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee){
		Employee result = mybatisService.findById(employee.getId());
		if(result == null) {
			throw new RecordNotFoundException(employee.getId());
		}
		mybatisService.update(employee);
	    return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@GetMapping(value = "findById/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) long employeeId){
		Employee result = mybatisService.findById(employeeId);
		if(result == null) {
			throw new RecordNotFoundException(employeeId);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@PostMapping("/batchInsert")
	public boolean batchInsert(@RequestParam("file") MultipartFile file) {
		if(file == null || file.isEmpty()) {
			return false;
		}
		List<String> list = new ArrayList<>();
		try (BufferedReader read = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
			String str;
			while((str = read.readLine()) != null) {
				list.add(str);
			}
			
		} catch (IOException e) {
			LOG.error("file:{} , error:{}",file.getName(), e);
		}
		return mybatisService.batchInsert(list);
	}
}
