package com.tgl.mvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.tgl.mvc.handler.ForbiddenHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.inMemoryAuthentication()
		.withUser("user").password("{noop}user").roles("USER")
		.and()
		.withUser("admin").password("{noop}admin").roles("ADMIN");
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		
		http
			.httpBasic()
			.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.GET,"/rest/**/findById/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/rest/**").hasRole("ADMIN")
			.anyRequest().authenticated()
			.and()
			.exceptionHandling().accessDeniedHandler(forbiddenHandler())
			.and()
			.csrf().disable();
			
	}
	
	@Bean
    public AccessDeniedHandler forbiddenHandler() {
        return new ForbiddenHandler();
    }
}
