package com.tgl.mvc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@EnableCaching
@ComponentScan({"com.tgl.mvc.*"})
@EnableAutoConfiguration(exclude=ErrorMvcAutoConfiguration.class)
@SpringBootApplication
@MapperScan("com.tgl.mvc.dao")
public class App{
  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }
}
