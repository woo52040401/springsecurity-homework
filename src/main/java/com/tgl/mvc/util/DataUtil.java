package com.tgl.mvc.util;

public class DataUtil {
	
	private DataUtil() {}
	
	public static String maskChName(String chName) {
		if(chName == null || chName.length() == 0) {
			return " ";
		}
		else if(chName.length() == 2 || chName.length() == 3) {
			return chName.replace(chName.charAt(1), '*');
		}
		else{
			String newChName = chName.replace(chName.charAt(1), '*');
			return newChName.replace(chName.charAt(2), '*');
		}
	}
	
	public static double bmi(double height, double weight) {
		return weight / Math.pow((height / 100), 2);
	}
}
