package com.tgl.mvc.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.mvc.dao.MybatisDao;
import com.tgl.mvc.exception.ValueNotValidException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;

@Service
public class MybatisService {
	
	@Autowired
	private MybatisDao mybatisDao;
	
	public long insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getTall(), employee.getWeight()));
		return mybatisDao.insert(employee);
	}
	
	public boolean delete(long employeeId) {
		return mybatisDao.delete(employeeId);
	}
	
	public int update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getTall(), employee.getWeight()));
		return mybatisDao.update(employee);
	}
	
	public Employee findById(long employeeId) {
		Employee result = mybatisDao.findById(employeeId);
		if(result == null) {
			return null;
		}
		result.setName(DataUtil.maskChName(result.getName()));
		return result;
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
	public boolean batchInsert(List<String> lists) {
		List<Employee> newLists = new ArrayList<>();
		for(String list : lists) {
			String[] strArray = list.split(",");
			if(lists == null || strArray.length != 6) {
				continue;
			}
			Employee employee = new Employee();
			if(!strArray[0].matches("^[\\u4e00-\\u9fa5.·]{0,}$")) {
				throw new ValueNotValidException("中文姓名: " + strArray[0]);
			}
			if(!strArray[1].matches("^[a-zA-Z]+-[a-zA-Z]+$")) {
				throw new ValueNotValidException("英文姓名: " + strArray[1]);
			}
			if(Integer.parseInt(strArray[2]) > 240 || Integer.parseInt(strArray[2]) < 40) {
				throw new ValueNotValidException("身高: " + strArray[2]);
			}
			if(Integer.parseInt(strArray[3]) > 150 || Integer.parseInt(strArray[3]) < 30) {
				throw new ValueNotValidException("體重: " + strArray[3]);
			}
			if(!strArray[4].matches("^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})$")) {
				throw new ValueNotValidException("Email : " + strArray[4]);
			}
			if(!strArray[5].matches("^[0-9]{4}")) {
				throw new ValueNotValidException("Phone : " + strArray[5]);
			}
			employee.setName(strArray[0]);
			employee.setEnName(strArray[1]);
			employee.setTall(Double.valueOf(strArray[2]));
			employee.setWeight(Double.valueOf(strArray[3]));
			employee.setEmail(strArray[4]);
			employee.setPhone(strArray[5]);
			employee.setBmi(DataUtil.bmi(employee.getTall(), employee.getWeight()));
			
			newLists.add(employee);
		}
		return mybatisDao.batchInsert(newLists);
	}
}
